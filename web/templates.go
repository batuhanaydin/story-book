package web

import "batuhanaydin.me/cyoa/data"

type templateData struct {
	Story      data.StoryArc
	FormErrors map[string]string
}
