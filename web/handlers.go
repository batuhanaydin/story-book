package web

import (
	"html/template"
	"log"
	"net/http"

	"batuhanaydin.me/cyoa/config"
	"batuhanaydin.me/cyoa/data"
)

func homeHandler(w http.ResponseWriter, r *http.Request) {
	story := data.NewStory(config.FileName)

	files := []string{
		"./ui/html/home.html",
		"./ui/html/base.html",
		"./ui/html/partials/navbar.html",
	}

	td, err := template.ParseFiles(files...)
	if err != nil {
		serverError(w, err)
		return
	}

	errors := map[string]string{}

	if r.Method == http.MethodPost {
		err := r.ParseForm()
		if err != nil {
			serverError(w, err)
			return
		}
		choice := r.Form.Get("choice")
		if _, ok := story[choice]; !ok {
			errors["choice"] = "Wrong choice, started again"
			err = td.Execute(w, &templateData{
				Story:      story[config.StarterArc],
				FormErrors: errors,
			})
			if err != nil {
				clientError(w, http.StatusBadRequest)
				return
			}
			return
		}
		err = td.Execute(w, &templateData{
			Story: story[choice],
		})
		if err != nil {
			log.Fatalln(err)
		}
		return
	}

	err = td.Execute(w, &templateData{
		Story: story[config.StarterArc],
	})
	if err != nil {
		log.Fatalln(err)
	}

}
