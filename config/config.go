package config

const (
	// FileName is the file that we insert the story
	FileName = "gopher.json"
	// StarterArc is where the story starts
	StarterArc = "intro"
)
