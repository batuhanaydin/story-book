package restful

import "net/http"

func routes() *http.ServeMux {
	mux := http.NewServeMux()
	mux.HandleFunc("/api/story", getStoryArc)

	return mux
}
