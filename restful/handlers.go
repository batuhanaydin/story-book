package restful

import (
	"encoding/json"
	"net/http"

	"batuhanaydin.me/cyoa/config"
	"batuhanaydin.me/cyoa/data"
)

func getStoryArc(w http.ResponseWriter, r *http.Request) {
	story := data.NewStory(config.FileName)

	choice := r.URL.Query().Get("choice")

	choice = choice[1 : len(choice)-1]
	if choice != "" {
		if _, ok := story[choice]; !ok {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		jsonData, err := json.Marshal(story[choice])
		if err != nil {
			w.WriteHeader(500)
			return
		}
		w.WriteHeader(200)
		w.Write(jsonData)
		return
	}

	jsonData, err := json.Marshal(story)
	if err != nil {
		w.WriteHeader(500)
		return
	}
	w.WriteHeader(200)
	w.Write(jsonData)
}
