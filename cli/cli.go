package cli

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"strconv"

	"batuhanaydin.me/cyoa/config"
	"batuhanaydin.me/cyoa/data"
)

// Play start the CLI version
func Play() {
	fileName := flag.String("file", config.FileName, "Filepath to import te story")
	starterArc := flag.String("start-from", config.StarterArc, "The arc to start from")
	flag.Parse()
	story := data.NewStory(*fileName)
	playTheStory(story, *starterArc)
}

func getUserInput(story data.Story, currentArc string) (chosenArcNumber int) {
	scanner := bufio.NewScanner(os.Stdin)
	err := *new(error)
	for scanner.Scan() {
		chosenArcNumber, err = strconv.Atoi(scanner.Text())
		if err != nil {
			log.Println("You should have write a number")
			continue
		}
		if chosenArcNumber <= 0 || chosenArcNumber > len(story[currentArc].Options) {
			log.Println("Not the numbers available")
			continue
		}
		break
	}
	return chosenArcNumber
}

func playTheStory(story data.Story, currentArc string) {
	fmt.Printf("\n\n")
	fmt.Println(story[currentArc].Title)
	fmt.Printf("\n\n")
	for _, storyText := range story[currentArc].Story {
		fmt.Printf("%s", storyText)
	}
	fmt.Println()
	if len(story[currentArc].Options) == 0 {
		fmt.Printf("\nThe story ends here...\n\n")
		os.Exit(0)
	}
	fmt.Printf("\nYou have %d choices...\n", len(story[currentArc].Options))
	for index, nextArc := range story[currentArc].Options {
		fmt.Printf("%d - %s\n", index+1, nextArc.Text)
	}
	chosenArcNumber := getUserInput(story, currentArc)
	playTheStory(story, story[currentArc].Options[chosenArcNumber-1].NextArc)
}
