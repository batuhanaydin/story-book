package data

import (
	"encoding/json"
	"io/ioutil"
	"log"
)

// StoryArc represents the story part
type StoryArc struct {
	Title   string   `json:"title"`
	Story   []string `json:"story"`
	Options []struct {
		Text    string `json:"text"`
		NextArc string `json:"arc"`
	} `json:"options"`
}

// Story is map that represents all of the story
type Story map[string]StoryArc

// NewStory returns a new story, reading from the JSON file
func NewStory(jsonFile string) Story {
	newStory := new(Story)
	file, err := ioutil.ReadFile(jsonFile)
	if err != nil {
		log.Fatalln(err)
	}
	json.Unmarshal(file, newStory)
	return *newStory
}
