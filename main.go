package main

import "batuhanaydin.me/cyoa/cli"

func main() {
	// uncomment only one of them at the same time
	//web.Start() // for web
	cli.Play() // for cli
	//restful.Start() // for rest api
}
